//
//  AppDelegate.h
//  DemoGit
//
//  Created by Rahul Lekurwale on 03/04/17.
//  Copyright © 2017 Rahul Lekurwale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

